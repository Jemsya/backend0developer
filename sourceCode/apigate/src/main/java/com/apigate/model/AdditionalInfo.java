
package com.apigate.model;

public class AdditionalInfo {

	private String tag;
	private String value;

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AdditionalInfo [tag=" + tag + ", value=" + value + "]";
	}

}
