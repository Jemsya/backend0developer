package com.apigate.model;

import java.util.Arrays;

public class Customer {

	private String msisdn;
	private String imsi;
	private String title;
	private String firstName;
	private String lastName;
	private String dob;
	private String identificationType;
	private String identificationNumber;
	private String onBehalfOf;
	private String purchaseCategoryCode;
	private String accountType;
	private String ownerType;
	private String status;
	private String requestIdentifier;
	private String responseIdentifier;
	private String address;
	private AdditionalInfo additionalInfo[];
	private String resourceURL;
	@Override
	public String toString() {
		return "Customer [msisdn=" + msisdn + ", imsi=" + imsi + ", title=" + title + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", dob=" + dob + ", identificationType=" + identificationType
				+ ", identificationNumber=" + identificationNumber + ", onBehalfOf=" + onBehalfOf
				+ ", purchaseCategoryCode=" + purchaseCategoryCode + ", accountType=" + accountType + ", ownerType="
				+ ownerType + ", status=" + status + ", requestIdentifier=" + requestIdentifier
				+ ", responseIdentifier=" + responseIdentifier + ", address=" + address + ", additionalInfo="
				+ Arrays.toString(additionalInfo) + ", resourceURL=" + resourceURL + "]";
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getIdentificationType() {
		return identificationType;
	}
	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public String getOnBehalfOf() {
		return onBehalfOf;
	}
	public void setOnBehalfOf(String onBehalfOf) {
		this.onBehalfOf = onBehalfOf;
	}
	public String getPurchaseCategoryCode() {
		return purchaseCategoryCode;
	}
	public void setPurchaseCategoryCode(String purchaseCategoryCode) {
		this.purchaseCategoryCode = purchaseCategoryCode;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRequestIdentifier() {
		return requestIdentifier;
	}
	public void setRequestIdentifier(String requestIdentifier) {
		this.requestIdentifier = requestIdentifier;
	}
	public String getResponseIdentifier() {
		return responseIdentifier;
	}
	public void setResponseIdentifier(String responseIdentifier) {
		this.responseIdentifier = responseIdentifier;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public AdditionalInfo[] getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(AdditionalInfo[] additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getResourceURL() {
		return resourceURL;
	}
	public void setResourceURL(String resourceURL) {
		this.resourceURL = resourceURL;
	}
	
	
 
}
