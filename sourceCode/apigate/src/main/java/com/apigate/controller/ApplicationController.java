package com.apigate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.apigate.model.Customer;
import com.apigate.service.ApplicationService;

@Controller
public class ApplicationController {

	@Autowired
	private ApplicationService applicationService;

	@RequestMapping(value = "/v1/customer/profile", method = RequestMethod.GET)
	public @ResponseBody Customer  getCustomerProfile(@RequestParam("msisdn") String mobileNumber) {

		return applicationService.getCustomerData(mobileNumber);
	}
	
//	@RequestMapping(value = "/v1/customer/attributes", method = RequestMethod.GET)
//	public ResponseEntity<String> getCustomerAttributes() {
//
//		 return new ResponseEntity<String>("Hello boss", HttpStatus.OK);
//	}

}